import {Injectable} from '@angular/core';

export interface Option<ValueType = string> {
	label: string;
	value: ValueType;
}

export enum AddressType {
	AddressNotAssignedBefore,
	FormationOfMemoryFromLandsOwnedByMunicipalities,
	FormationOfAMemoryByPartitioningAMemory,
	FormationOfMemoryByCombiningMemory,
	FormationOfMemoryByAllocatingMemory,
	FormationOfMemoryByRedistributionOfMemory,
}

export enum ObjectType {
	Building,
	LandPlot,
	Room,
	Construction,
	ParkingSpace,}

@Injectable({
	providedIn: 'root'
})
export class SelectionsService {
	private AddressTypeOptions: Array<Option<AddressType>> = [
		{
			label: "Адрес не присваивался ранее",
			value: AddressType.AddressNotAssignedBefore
		},
		{
			label: "Образование ЗУ из земель, находящихся в муниципальной собственности\n",
			value: AddressType.FormationOfMemoryFromLandsOwnedByMunicipalities
		},
		{
			label: "Образование ЗУ путем раздела ЗУ",
			value: AddressType.FormationOfAMemoryByPartitioningAMemory
		},
		{
			label: "Образование ЗУ путем объединения ЗУ\n",
			value: AddressType.FormationOfMemoryByCombiningMemory
		},
		{
			label: "Образование ЗУ путем выдела ЗУ\n",
			value: AddressType.FormationOfMemoryByAllocatingMemory
		},
		{
			label: "Образование ЗУ путем перераспределения ЗУ",
			value: AddressType.FormationOfMemoryByRedistributionOfMemory
		},
	]

	private AddressObjectType: Array<Option<ObjectType>> = [
		{
			label: "Здание",
			value: ObjectType.Building
		},
		{
			label: "Земельный участок",
			value: ObjectType.LandPlot
		},
		{
			label: "Помещение",
			value: ObjectType.Room
		},
		{
			label: "Сооружение",
			value: ObjectType.Construction
		},
		{
			label: "Машино-место",
			value: ObjectType.ParkingSpace
		}
	]

	getAddressObjectType() {
		return this.AddressObjectType
	}

	getAddressTypeOptions() {
		return this.AddressTypeOptions
	}

	findTitleBuilding(id: number) {
		return this.AddressObjectType[id].label;
	}

	findTypeOption(id: number) {
		return this.AddressTypeOptions[id].label;
	}
}
