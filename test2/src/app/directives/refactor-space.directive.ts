import {
	Directive,
	HostListener,
} from '@angular/core';
import {FormControlName} from "@angular/forms";

@Directive({
	selector: '[appRefactorSpace]',
	})
export class RefactorSpaceDirective {

	constructor(private control: FormControlName) {
	}

	@HostListener('focusout', ['$event.target.value'])
	onInputChange() {
		const newValue = this.control.value.trim()
		this.control.control.setValue(newValue)
	}
}
