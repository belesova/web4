import {ChangeDetectionStrategy, Component} from '@angular/core';
import {AddressType, ObjectType, Option, SelectionsService} from "../../service/selections.service";
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";


export enum typeOfAddressing {
	AddressNotAssignedBefore = 0,
	FormationOfMemoryFromLandsOwnedByMunicipalities = 1,
	FormationOfAMemoryByPartitioningAMemory = 2,
	FormationOfMemoryByCombiningMemory= 3,
	FormationOfMemoryByAllocatingMemory = 4,
	FormationOfMemoryByRedistributionOfMemory = 5,
}

@Component({
	selector: 'app-services',
	templateUrl: './services.component.html',
	styleUrls: ['./services.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServicesComponent {
	buildingTitle: string = 'Земельный участок';
	typeBuildingTitle: string = '';

	index: number = 6;

	arrChooseBuilding: Array<Option<ObjectType>> = this.selectionsService.getAddressObjectType();
	typeOfAddressing: Array<Option<AddressType>> = this.selectionsService.getAddressTypeOptions();

	mainForm: FormGroup;
	constructor(private selectionsService: SelectionsService,
				private formBuilder: FormBuilder) {
		this.mainForm = this.formBuilder.group({
				"building": ['', Validators.required],
				"addressType": ['', Validators.required],
				"landPort": [0,
					[Validators.pattern('^[0-9]+$'),
						Validators.min(0),
						Validators.required]],
				"addition": '',
			}
		)

		this.mainForm.valueChanges.subscribe((val)=>
		{
			console.log(this.mainForm.controls)
		})
	}

	changeTypeOfAddressing(id: number) {
		//if (id !== typeOfAddressing.FormationOfMemoryFromLandsOwnedByMunicipalities
		//	&& this.mainForm.controls['district'])
		//{
		//	this.mainForm.removeControl('district', {
		//		emitEvent: false
		//	})
		//}
		//
		//if ((id !== typeOfAddressing.AddressNotAssignedBefore
		//		&& id !== typeOfAddressing.FormationOfAMemoryByPartitioningAMemory
		//		&& id !== typeOfAddressing.FormationOfMemoryByAllocatingMemory)
		//	&& this.mainForm.controls['number']
		//	&& this.mainForm.controls['address'])
		//{
		//	this.mainForm.removeControl('number', {
		//		emitEvent: false
		//	})
		//	this.mainForm.removeControl('address', {
		//		emitEvent: false
		//	})
		//}
		//
		//if ((id !== typeOfAddressing.FormationOfMemoryFromLandsOwnedByMunicipalities
		//		&& id !== typeOfAddressing.FormationOfAMemoryByPartitioningAMemory
		//		&& id !== typeOfAddressing.FormationOfMemoryByAllocatingMemory
		//		&& id!= typeOfAddressing.FormationOfMemoryByRedistributionOfMemory)
		//	&& this.mainForm.controls['landPort'])
		//{
		//	this.mainForm.removeControl('landPort', {
		//		emitEvent: false
		//	})
		//}
		//if ((id !== typeOfAddressing.FormationOfMemoryFromLandsOwnedByMunicipalities
		//		&& id !== typeOfAddressing.FormationOfAMemoryByPartitioningAMemory
		//		&& id !== typeOfAddressing.FormationOfMemoryByAllocatingMemory
		//		&& id!= typeOfAddressing.FormationOfMemoryByRedistributionOfMemory)
		//	&& !this.mainForm.controls['landPort'])
		//{
		//	this.mainForm.addControl("landPort",
		//		new FormControl(
		//			[0,
		//			[Validators.pattern('^[0-9]+$'),
		//			Validators.min(0),
		//			Validators.required]]), {
		//				emitEvent: false
		//		})
		//}
		//
		//if ((id !== typeOfAddressing.FormationOfMemoryByCombiningMemory
		//		&& id !== typeOfAddressing.FormationOfMemoryByRedistributionOfMemory
		//	)
		//	&& this.mainForm.controls['cardsLand'])
		//{
		//	(this.mainForm.controls['cardsLand'] as FormArray).clear();
		//}
		//
		//this.index = id+1;
		//this.typeBuildingTitle = this.selectionsService.findTypeOption(id);
	}

	changeBuilding(id: number) {
		this.buildingTitle = this.selectionsService.findTitleBuilding(id);
	}
}
