import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ServicesComponent} from "./pages/services/services.component";
import {AppComponent} from "./app.component";

const routes: Routes = [
	{component: ServicesComponent, path: "services"},
	{component: AppComponent, path: ""},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
