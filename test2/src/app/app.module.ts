import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ServicesComponent} from './pages/services/services.component';
import {ChooseBuildingFormComponent} from './components/forms/choose-building-form/choose-building-form.component';
import {HeaderComponent} from './components/header/header.component';
import {CommonModule} from "@angular/common";
import {IconComponent} from './components/ui/icon/icon.component';
import {ButtonWithIconComponent} from './components/ui/button-with-icon/button-with-icon.component';
import {ProgressBarComponent} from './components/progress-bar/progress-bar.component';
import {FooterComponent} from './components/footer/footer.component';
import {InputWithIconComponent} from './components/ui/input-with-icon/input-with-icon.component';
import {InputCounterComponent} from './components/ui/input-counter/input-counter.component';
import {CardLandPlotComponent} from './components/ui/card-land-plot/card-land-plot.component';
import {RadioButtonGroupComponent} from './components/ui/radio-button-group/radio-button-group.component';
import { NumberAddressFormComponent } from './components/forms/ways/number-address-form/number-address-form.component';
import { DistrictFormComponent } from './components/forms/ways/district-form/district-form.component';
import { LandPortsFormComponent } from './components/forms/ways/land-ports-form/land-ports-form.component';
import {IMaskModule} from "angular-imask";
import { RefactorSpaceDirective } from './directives/refactor-space.directive';

@NgModule({
	declarations: [
		AppComponent,
		ServicesComponent,
		ChooseBuildingFormComponent,
		HeaderComponent,
		IconComponent,
		ButtonWithIconComponent,
		ProgressBarComponent,
		FooterComponent,
		InputWithIconComponent,
		InputCounterComponent,
		CardLandPlotComponent,
		RadioButtonGroupComponent,
  NumberAddressFormComponent,
  DistrictFormComponent,
  LandPortsFormComponent,
  RefactorSpaceDirective,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		IMaskModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
