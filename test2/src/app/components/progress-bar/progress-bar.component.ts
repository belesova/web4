import {Component, ChangeDetectionStrategy} from '@angular/core';

@Component({
	selector: 'app-progress-bar',
	templateUrl: './progress-bar.component.html',
	styleUrls: ['./progress-bar.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgressBarComponent {
}
