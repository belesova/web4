import {ChangeDetectionStrategy, Component, EventEmitter, forwardRef, Input, Output} from '@angular/core';
import {ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
	selector: 'app-choose-building-form',
	templateUrl: './choose-building-form.component.html',
	styleUrls: ['./choose-building-form.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => ChooseBuildingFormComponent),
		multi: true
	}]
}) 
export class ChooseBuildingFormComponent implements ControlValueAccessor{
	@Input() title: string = 'Вид объекта адресации';
	@Input() arr: Array<any> = [];
	@Input() status: "PRIMARY" | "SECONDARY" = "PRIMARY";
	@Output() statusForm = new EventEmitter();
	form: FormGroup;
	value: string = '';

	constructor(private formBuilder: FormBuilder) {
		this.form = this.formBuilder.group({
			selection: '1'
		})
		this.form.get("selection")?.valueChanges.subscribe((value) => {
			this.setStatus(value)
		})
	}

	setStatus(id: string) {
		this.statusForm.emit(id);
		this.value = id;
		this.onChangeCallback(this.value);
		this.onTouchedCallback();
	}

	onChangeCallback = (v: any) => {};
	onTouchedCallback = () => {};

	registerOnChange(fn: any): void {
		this.onChangeCallback = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouchedCallback = fn;
	}

	writeValue(val: string): void {
		this.value = val;
	}
}
