import {Component, ChangeDetectionStrategy, Input, OnInit} from '@angular/core';
import {
	FormControl,
	FormGroup,
	Validators
} from "@angular/forms";

@Component({
	selector: 'app-number-address-form',
	templateUrl: './number-address-form.component.html',
	styleUrls: ['../form.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NumberAddressFormComponent implements OnInit {
	@Input() form!: FormGroup;

	ngOnInit(): void {
		this.form?.addControl('number', new FormControl('',
				[Validators.required, this.numberValidator]), {
				emitEvent: false
			});
		this.form?.addControl('address', new FormControl('', Validators.required), {
			emitEvent: false
		});
    }

	numberValidator = (control: FormControl) => {
		const validLength = control.value.length === 15;
		if (!validLength) {
			return { validNumber: false}
		}
		return null;
	}

	valid(controlName: string) {
		return this.form.controls[controlName].invalid && this.form.controls[controlName].touched;
	}
}
