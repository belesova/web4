import {Component, ChangeDetectionStrategy, OnInit, Input} from '@angular/core';
import {
	FormArray,
	FormControl,
	FormGroup,
	Validators
} from "@angular/forms";

export interface Item {
	number: string,
	address: string
}

@Component({
	selector: 'app-land-ports-form',
	templateUrl: './land-ports-form.component.html',
	styleUrls: ['../form.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandPortsFormComponent implements OnInit{
	itemClean: Item = {
		number: '',
		address: ''
	}

	values: Array<Item> = [];
	@Input() form!: FormGroup;

	ngOnInit(): void {

		this.form.addControl('cardsLand', new FormArray([
			new FormGroup({
				address: new FormControl(''),
				number: new FormControl(''),
				//info: new FormControl(this.itemClean, Validators.required)
			})
		]))



		this.form.valueChanges.subscribe(value => console.log(this.form.value))
	}

	addCardLand() {
		const info = new FormControl(this.itemClean);

		(<FormArray>this.form.controls["cardsLand"]).push(new FormGroup({address: new FormControl(''),
			number: new FormControl(''),}));
	}

	deleteCardLand(index: number) {
		(<FormArray>this.form.controls["cardsLand"]).removeAt(index)
	}

	getCardsLandInfo() {
		return this.form.controls["cardsLand"] as FormArray;
	}
}
