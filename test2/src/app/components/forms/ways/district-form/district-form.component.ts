import {Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

interface Item {
	district: number;
}

@Component({
	selector: 'app-district-form',
	templateUrl: './district-form.component.html',
	styleUrls: ['../form.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DistrictFormComponent implements OnInit {
	@Input() form!: FormGroup;
	value: Item = {
		district: 1
	};

	ngOnInit(): void {
		this.form?.addControl('district',
			new FormControl('', Validators.required),
			{
			emitEvent: false
		});
	}
}
