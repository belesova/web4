import {
	Component,
	ChangeDetectionStrategy,
	Input,
	forwardRef, OnInit,
} from '@angular/core';
import {
	ControlContainer,
	ControlValueAccessor,
	FormBuilder,
	FormControl, FormControlName,
	FormGroup, FormGroupName,
	NG_VALUE_ACCESSOR,
	Validators
} from "@angular/forms";
import {Item} from "../../forms/ways/land-ports-form/land-ports-form.component";

@Component({
	selector: 'app-card-land-plot',
	templateUrl: './card-land-plot.component.html',
	styleUrls: ['./card-land-plot.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => CardLandPlotComponent),
		multi: true
	}]
})
export class CardLandPlotComponent implements ControlValueAccessor{
	@Input() numberLP: number = 0;
	form: FormGroup;

	value: Item = {
		address: '',
		number: ''
	}

	constructor(private formBuilder: FormBuilder) {
		this.form = this.formBuilder.group({
			"address": ["", Validators.required, null, {
				emitEvent: false
			}],
			"number": ["", [Validators.required, this.numberValidator], null, {
				emitEvent: false
			}],
		})
		this.form.valueChanges.subscribe((val) => {
			this.onChange( {
				address: val.address,
				number: val.number
			});
		})
	}
	onChange(val: Item) {
		this.value = val;
		this.onChangeCallback(this.value);
		this.onTouchedCallback();
	}

	onChangeCallback = (v: any) => {
	};
	onTouchedCallback = () => {
	};

	writeValue(val: Item): void {
		this.value = val;
	}

	registerOnChange(fn: any): void {
		this.onChangeCallback = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouchedCallback = fn;
	}


	numberValidator = (control: FormControl) => {
		const validLength = control.value.length === 15;
		if (!validLength) {
			return {validNumber: false}
		}
		return null;
	}

	valid(controlName: string) {
		return this.form.controls[controlName].invalid && this.form.controls[controlName].touched;
	}
}
