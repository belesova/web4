import {Component, ChangeDetectionStrategy, Input, forwardRef} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
	selector: 'app-input-with-icon',
	templateUrl: './input-with-icon.component.html',
	styleUrls: ['./input-with-icon.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => InputWithIconComponent),
		multi: true
	}]
})
export class InputWithIconComponent implements ControlValueAccessor {
	@Input() placeholder: string = '';
	@Input() invalid: boolean = false;
	@Input() type: 'text' | 'number' = 'text';
	@Input() hasMaskNumber: boolean = false;

	input: FormControl = new FormControl('')

	maskVal = {
		mask: '00{:}00{:}0000000{:}0'
	}

	onChange: any = (value: any) => {
		this.input.setValue(value);
		this.onChangeCallback(this.input.value);
		this.onTouchedCallback();
	}

	onChangeCallback = (v: any) => {
	};
	onTouchedCallback = () => {
	};


	writeValue(value: any) {
		this.input.setValue(value)
	}

	registerOnChange(fn: any): void {
		this.onChangeCallback = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouchedCallback = fn;
	}
}
