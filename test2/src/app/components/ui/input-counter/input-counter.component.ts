import {Component, ChangeDetectionStrategy, forwardRef, Input} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
	selector: 'app-input-counter',
	templateUrl: './input-counter.component.html',
	styleUrls: ['./input-counter.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => InputCounterComponent),
		multi: true
	}]
})
export class InputCounterComponent implements ControlValueAccessor {
	@Input() placeholder: string = '';
	@Input() invalid: boolean = false;

	counter: FormControl = new FormControl(0);

	changeValue(val: number) {
		this.counter.setValue(val);
		this.onChangeCallback(this.counter.value)
		this.onTouchedCallback();
	}

	increaseValue(val: number) {
		const currentValue = this.counter.value;
		this.counter.setValue(currentValue + val)
		this.onChangeCallback(this.counter.value)
		this.onTouchedCallback();
	}

	onChangeCallback = (v: any) => {};
	onTouchedCallback = () => {};

	registerOnChange(fn: any): void {
		this.onChangeCallback = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouchedCallback = fn;
	}

	writeValue(val: any): void {
		if (val === null) {
			this.counter.setValue(0);
			this.onChangeCallback(this.counter.value)
			return
		}
		this.counter.setValue(0);
		this.onChangeCallback(this.counter.value)
	}
}
