import {Component, ChangeDetectionStrategy, Input} from '@angular/core';

@Component({
	selector: 'app-button-with-icon',
	templateUrl: './button-with-icon.component.html',
	styleUrls: ['./button-with-icon.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonWithIconComponent {
	@Input() size: string = '24px';
	@Input() text: string = '';
	@Input() name: string = '';
	@Input() active: boolean = true;
	@Input() status: 'PRIMARY' | 'SECONDARY' | 'THIRTY' = 'PRIMARY';
	@Input() reverse: boolean = false;
}

