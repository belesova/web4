import {Component, ChangeDetectionStrategy, Input, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
	selector: 'app-radio-button-group',
	templateUrl: './radio-button-group.component.html',
	styleUrls: ['./radio-button-group.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => RadioButtonGroupComponent),
		multi: true
	}]

})
export class RadioButtonGroupComponent implements ControlValueAccessor{
	@Input() status: 'PRIMARY'|'SECONDARY' = 'PRIMARY'
	innerValue: string = '';

	@Input() arrayRadio = [
		{
			value: '1',
			label: '1'
		},
		{
			value: '2',
			label: '2'
		},
		{
			value: '3',
			label: '3'
		}
	];

	onChangeCallback = (v: any) => {};
	onTouchedCallback = () => {};

	registerOnChange(fn: any): void {
		this.onChangeCallback = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouchedCallback = fn;
	}

	writeValue(value: string): void {
		if (this.innerValue !== value) {
			this.innerValue = value
		}
	}

	onChange(value: string): void{
		this.innerValue = value;
		this.onChangeCallback(value);
		this.onTouchedCallback();
	}
}
