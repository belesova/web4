import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FifthTaskComponent } from './pages/fifth-task/fifth-task.component';
import { FirstTaskComponent } from './pages/first-task/first-task.component';
import { ThirdTaskComponent } from './pages/third-task/third-task.component';
import { FourthTaskComponent } from './pages/fourth-task/fourth-task.component';
import { VideoComponent } from './pages/secondTask/video/video.component';
import { HomeComponent } from './pages/secondTask/home/home.component';
import { VideoStopDirective } from './directives/video-stop.directive';
import { FocusWithinDirective } from './directives/focus-within/focus-within.directive';
import { VirtualizeTaskComponent } from './pages/virtualize-task/virtualize-task.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { InfiniteTableComponent } from './pages/infinite-table/infinite-table.component';

@NgModule({
	declarations: [
		AppComponent,
		ThirdTaskComponent,
		FourthTaskComponent,
		FifthTaskComponent,
		FirstTaskComponent,
  VideoComponent,
  HomeComponent,
  VideoStopDirective,
  FocusWithinDirective,
  VirtualizeTaskComponent,
  InfiniteTableComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		ScrollingModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
