import { AfterContentInit, Directive, Inject } from '@angular/core';
import { PAGE_VISIBILITY } from '../pages/secondTask/home/home.component';
import { fromEvent, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

@Directive({
  selector: '[tkVideoStop]'
})
export class VideoStopDirective{
	hidden!: any
	visibilityChange!: any
	video!: HTMLVideoElement | null;

}
