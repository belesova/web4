import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { FourthTaskComponent } from './pages/fourth-task/fourth-task.component';
import { FifthTaskComponent } from './pages/fifth-task/fifth-task.component';
import { FirstTaskComponent } from './pages/first-task/first-task.component';
import { ThirdTaskComponent } from './pages/third-task/third-task.component';
import { VideoComponent } from './pages/secondTask/video/video.component';
import { HomeComponent } from './pages/secondTask/home/home.component';
import { VirtualizeTaskComponent } from './pages/virtualize-task/virtualize-task.component';
import { InfiniteTableComponent } from './pages/infinite-table/infinite-table.component';

const routes: Routes = [
	{
		component: FirstTaskComponent,
		path: '1'
	},
	{
		component: VideoComponent,
		path: '2/v',
	},
	{
		component: HomeComponent,
		path: '2',
	},
	{
		component: ThirdTaskComponent,
		path: '3',
	},
	{
		component: FourthTaskComponent,
		path: '4',
	},
	{
		component: FifthTaskComponent,
		path: '5',
	},

	{
		component: VirtualizeTaskComponent,
		path: 'virtualize',
	},
	{
		component: InfiniteTableComponent,
		path: '',
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
