import { Component } from '@angular/core';

@Component({
  selector: 'tk-virtualize-task',
  templateUrl: './virtualize-task.component.html',
  styleUrls: ['./virtualize-task.component.sass']
})
export class VirtualizeTaskComponent {
	items = new Array<string>();
	constructor() {
		for (let loop = 0; loop < 100000; loop++) {
			this.items.push('item№' + loop)
		}
	}
}
