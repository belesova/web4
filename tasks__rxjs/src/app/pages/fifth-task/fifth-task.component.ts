import { Component, OnDestroy } from '@angular/core';
import { scan, Subject, Subscription, takeWhile, timer } from 'rxjs';

interface Time {
	minutes: number,
	seconds: number
}

@Component({
	selector: 'tk-fifth-task',
	templateUrl: './fifth-task.component.html',
	styleUrls: ['./fifth-task.component.sass']
})
export class FifthTaskComponent implements OnDestroy {
	currentTime: Time = {
		seconds: 30,
		minutes: 2
	};

	time: Subject<Time> = new Subject<Time>();

	sub!: Subscription;

	start() {
		this.sub = timer(0, 1000).pipe(
			scan(acc => --acc, 150),
			takeWhile(seconds => seconds >= 0)
		).subscribe({
			next: value => {
				this.time.next(
					this.currentTime = {
						minutes: Math.trunc(value / 60),
						seconds: Math.trunc(value % 60)
					}
				)
			}
		});
	}

	restart() {
		this.sub.unsubscribe()
		this.start()
	}

	ngOnDestroy(): void {
		this.sub.unsubscribe()
		this.time.unsubscribe()
	}
}
