import { Component, Inject, OnDestroy } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Observable, Subscription } from 'rxjs';

@Component({
	selector: 'tk-third-task',
	templateUrl: './third-task.component.html',
	styleUrls: ['./third-task.component.sass']
})
export class ThirdTaskComponent implements OnDestroy {
	constructor(
		// Returns username or throws an error
		@Inject(LoginService) private readonly service: Observable<string>
	) {
	}

	name!: string;
	error!: string;
	login!: Subscription;
	disabledBtn!: boolean;

	click() {
		this.error = ''
		this.name = ''
		this.disabledBtn = true;
		this.login = this.service
			.subscribe(
				{
					next: value => {
						this.name = value
						this.disabledBtn = false
					},
					error: err => {
						this.error = 'Ошибка авторизации'
						this.disabledBtn = false
					}
				}
			)
	}

	ngOnDestroy(): void {
		this.login.unsubscribe()
	}
}
