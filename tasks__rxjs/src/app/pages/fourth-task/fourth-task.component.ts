import {Component, Inject, OnDestroy} from '@angular/core';
import {LoadingService} from "../../services/loading.service";
import { startWith, Subject, Subscription, takeUntil } from 'rxjs';

@Component({
	selector: 'tk-fourth-task',
	templateUrl: './fourth-task.component.html',
	styleUrls: ['./fourth-task.component.sass']
})
export class FourthTaskComponent implements OnDestroy{
	constructor(
		@Inject(LoadingService) private loadingService: LoadingService
	) {}

	perCent: any = 0;
	result: boolean = false;
	loading!: boolean;

	sub!: Subscription;

	destroy$ = new Subject();

	onButtonClick() {
		this.sub = this.loadingService
			.load()
			.pipe(
				takeUntil(this.destroy$),
				startWith(
					this.loading = true,
					this.result = false,
					this.perCent = 0,)
			)
			.subscribe({
				next: value => this.perCent = value,
				complete: () => {
					this.result = true
					this.loading = false
				}
			})
	}

	ngOnDestroy(): void {
		this.destroy$.next(null)
		this.destroy$.complete();
	}
}
