import { Component } from '@angular/core';

@Component({
	selector: 'tk-first-task',
	templateUrl: './first-task.component.html',
	styleUrls: ['./first-task.component.sass']
})
export class FirstTaskComponent{
	focused: Element | null = null;

	get name(): string {
		return this.focused ? this.focused.tagName : 'null';
	}

	onFocusWithin(focused: Element | null) {
		this.focused = focused;
	}
}
