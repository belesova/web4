import { Component, ViewEncapsulation } from '@angular/core';

@Component({
	selector: 'tk-infinite-table',
	templateUrl: './infinite-table.component.html',
	encapsulation: ViewEncapsulation.None,
	styleUrls: ['./infinite-table.component.sass']
})
export class InfiniteTableComponent {
	items = new Array<string>();
	constructor() {
		for (let loop = 0; loop < 150000; loop++) {
			this.items.push('item№' + loop)
		}
	}
}
