import { Component, Inject } from '@angular/core';
import { VideoStopDirective } from '../../../directives/video-stop.directive';
import { Observable } from 'rxjs';
import { PAGE_VISIBILITY } from '../home/home.component';

@Component({
	selector: 'tk-video',
	templateUrl: './video.component.html',
	styleUrls: ['./video.component.sass'],
	providers: [VideoStopDirective]
})
export class VideoComponent {

	videoLink = document.querySelector('video')
	constructor(
		@Inject(PAGE_VISIBILITY)
		readonly pageVisibility$: Observable<boolean>
	) {
	}

;


}
