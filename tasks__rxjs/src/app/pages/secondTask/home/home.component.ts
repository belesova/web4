import { Component, Inject, inject, InjectionToken } from '@angular/core';
import { fromEvent, Observable, share, startWith } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { distinctUntilChanged, map } from 'rxjs/operators';

export const PAGE_VISIBILITY = new InjectionToken<Observable<boolean>>(
	"Shared Observable based on `document visibility changed`",
	{
		factory: () => {
			const documentRef = inject(DOCUMENT);

			return fromEvent(documentRef, "visibilitychange").pipe(
				startWith(0),
				map(() => documentRef.visibilityState !== "hidden"),
				distinctUntilChanged(),
				share()
			);
		}
	}
);


export const VIDEO_VISIBILITY = new InjectionToken<Observable<boolean>>(
	"Shared Observable (visibility_video) based on `document visibility changed`",
	{
		factory: () => {
			const documentRef = inject(DOCUMENT);

			return fromEvent(documentRef, "visibilitychange").pipe(
				startWith(0),
				map(() => documentRef.visibilityState !== "hidden"),
				distinctUntilChanged(),
				share()
			);
		}
	}
);


@Component({
	selector: 'tk-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.sass']
})
export class HomeComponent{
	constructor(
		@Inject(PAGE_VISIBILITY)
		readonly pageVisibility$: Observable<boolean>,
		@Inject(VIDEO_VISIBILITY)
		readonly videoVisibility$: Observable<boolean>
	) {
	}
}
