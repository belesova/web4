import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CloseGuard implements CanDeactivate<string> {
	canDeactivate(component: string, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

		window.onbeforeunload = function(e) {
			return 'Dialog text here.';
		};

		return confirm('При переходе со страницы профиля вы потеряете введеные данные');
	}
}
