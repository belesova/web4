import {HostListener, Injectable, OnDestroy} from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClosePageGuard implements CanDeactivate<unknown> {

	@HostListener('window:beforeunload')
	canDeactivate(
		component: unknown,
		currentRoute: ActivatedRouteSnapshot,
		currentState: RouterStateSnapshot,
		nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

		window.onbeforeunload = function(e) {
			const dialogText = 'Хотите выйти?';
			e.returnValue = dialogText;
			return dialogText;
		};

		return confirm('Хотите выйти?');
	}
}
