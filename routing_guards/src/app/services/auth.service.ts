import {Injectable} from '@angular/core';

export interface IUser {
	login: string,
	password: string,
	role?: 'USER' | 'ADMIN' | 'GUEST';
}

export enum listRoles {
	'USER',
	'ADMIN',
	'GUEST'
}

@Injectable({
	providedIn: 'root'
})

export class AuthService {
	isAuth: boolean = false;
	isRole: string = 'GUEST';
	token: string = '21312423432423'

	data: Array<IUser> = [
		{
			login: 'user1',
			password: 'qwerty123',
			role: "USER"
		},
		{
			login: "user2",
			password: "qwerty1",
			role: "ADMIN"
		},
	]

	signIn(role: string, user?: IUser) {
		if (role === 'GUEST') {
			this.isRole = 'GUEST'
			this.isAuth = true
			return
		}

		this.data.filter((value => {
			if (user?.login == value.login
				&& user?.password == value.password) {
				this.isAuth = true
				this.isRole = role
				localStorage.setItem('token', this.token)
				return true;
			} else {
				return false;
			}
		}))
	}

	logOut() {
		this.isAuth = false;
		localStorage.removeItem('token')
	}

	constructor() {
	}
}
