import {Component} from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";

@Component({
	selector: 'tk-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.sass']
})
export class ProfileComponent {
	role!: string;

	constructor(private authService: AuthService,
				private router: Router) {
		this.role = this.authService.isRole
	}

	logout() {
		this.authService.logOut()
		this.router.navigateByUrl('/')
	}
}
