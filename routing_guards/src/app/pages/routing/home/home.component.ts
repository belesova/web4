import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService, IUser} from "../../../services/auth.service";
import {Router} from "@angular/router";
@Component({
	selector: 'tk-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.sass']
})
export class HomeComponent {
	form: FormGroup;

	constructor(private formBuilder: FormBuilder,
				private authService: AuthService,
				private router: Router) {
		this.form = this.formBuilder.group({
			login: ['', Validators.required],
			password: ['', Validators.required]
		})
	}

	login(role: string) {
		if (role === 'GUEST') {
			this.authService.signIn('GUEST')
			this.router.navigateByUrl('/profile')
		} else {
			const userData: IUser = {
				login: this.form.controls['login'].value,
				password: this.form.controls['password'].value,
			}
			this.authService.signIn(role, userData);
			this.authService.isAuth ? this.router.navigateByUrl('/profile') : console.log('ошибка авторизации')

		}
	}
}
