import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./pages/routing/home/home.component";
import {ProfileComponent} from "./pages/routing/profile/profile.component";
import {UserBlockComponent} from "./pages/user-block/user-block.component";
import {AdminBlockComponent} from "./pages/routing/admin-block/admin-block.component";
import {GuestBlockComponent} from "./pages/routing/guest-block/guest-block.component";
import {AuthorizationGuard} from "./guards/authorization.guard";
import {AuthenticationGuard} from "./guards/authentication.guard";
import {listRoles} from "./services/auth.service";
import {CloseGuard} from "./guards/close.guard";
import {ClosePageGuard} from "./guards/close-page.guard";

const routes: Routes = [
	{
		component: HomeComponent,
		path: '',
	},
	{
		component: ProfileComponent,
		path: 'profile',
		canActivate: [AuthenticationGuard],
		canDeactivate: [CloseGuard],
		data: {
			roles: [listRoles.USER, listRoles.GUEST, listRoles.ADMIN]
		}
	},
	{
		component: UserBlockComponent,
		path: 'user_page',
		canActivate: [AuthorizationGuard, AuthenticationGuard],
		data: {
			roles: [listRoles.USER]
		}
	},
	{
		component: AdminBlockComponent,
		path: 'admin_page',
		canActivate: [AuthorizationGuard, AuthenticationGuard],
		data: {
			roles: [listRoles.ADMIN]
		}
	},
	{
		component: GuestBlockComponent,
		path: 'guest_page',
		canActivate: [AuthorizationGuard],
		canDeactivate: [ClosePageGuard],
		data: {
			roles: [listRoles.GUEST]
		}	},
	{
		path: '**',
		redirectTo: ''
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
