import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AdminBlockComponent} from './pages/routing/admin-block/admin-block.component';
import {HomeComponent} from './pages/routing/home/home.component';
import {UserBlockComponent} from './pages/user-block/user-block.component';
import {ProfileComponent} from './pages/routing/profile/profile.component';
import {HeaderComponent} from './components/header/header.component';
import {ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {GuestBlockComponent} from './pages/routing/guest-block/guest-block.component';
@NgModule({
  declarations: [
    AppComponent,
    AdminBlockComponent,
    HomeComponent,
    UserBlockComponent,
    ProfileComponent,
    HeaderComponent,
    GuestBlockComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
